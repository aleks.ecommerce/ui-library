import $ from './core';
import './modules/display';
import './modules/classes';
import './modules/handlers';
import './modules/actions';
import './modules/effects';
import './modules/components/dropdown';
import './modules/components/modal';
import './modules/components/tabs';
import './modules/components/accordeon';
import './modules/components/carousel';
import './modules/components/carousel_extended';
import './services/requests';

export default $;